<?php

namespace Pta\Backup\Repositories;

use Pta\Backup\Models\Backup;
use Illuminate\Container\Container;
use Illuminate\Support\Facades\Artisan;

class BackupRepository implements BackupRepositoryInterface
{
    protected $app;
    
    protected $model;

    public function __construct(Container $app, Backup $model)
    {
        $this->app = $app;

        $this->model = $model;

        config()->set('backup-manager.s3.key', env('S3_KEY', 'your-key') );
        config()->set('backup-manager.s3.secret', env('S3_SECRET', 'your-secret') );
        config()->set('backup-manager.s3.region', env('S3_REGION', 'your-region') );
        config()->set('backup-manager.s3.bucket', env('S3_BUCKET', 'your-bucket'));
        config()->set('backup-manager.s3.root', '');

    }

    public function makeBackup($driver, $database)
    {
        $fileName = 'db/' .date('Y-m-d-His');
        Artisan::call('db:backup', [
            '--database' => $database,
            '--destination' => $driver,
            '--destinationPath' => $fileName,
            '--compression' => 'gzip',
            '--no-interaction' => true
        ]);

        $this->model->create([
            'driver' => $driver,
            'name' => $fileName.'.gz',
        ]);
    }

    public function getBackups()
    {
        return $this->model->orderBy('id', 'desc')->get();
    }
    
    public function getBackup($id)
    {
        return $this->model->find($id);
    }
}
