<?php

namespace Pta\Backup\Repositories;

interface BackupRepositoryInterface
{
	public function makeBackup($driver, $database);

	public function getBackups();
	
	public function getBackup($id);
}
