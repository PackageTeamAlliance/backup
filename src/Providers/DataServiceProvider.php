<?php

namespace Pta\Backup\Providers;

use Pta\Backup\Models\Backup;
use Illuminate\Support\ServiceProvider;
use Pta\Backup\Repositories\BackupRepository;
use Pta\Backup\Repositories\BackupRepositoryInterface;

class DataServiceProvider extends ServiceProvider
{
    /**
     * {@inheritDoc}
     */
    public function boot()
    {
        //
    }
    
    /**
     * {@inheritDoc}
     */
    public function register()
    {
        //register backup repository
        $this->app->bind(BackupRepositoryInterface::class, function ($app) {
            return new BackupRepository($app, new Backup, null);
        });
    }
}
