<?php

namespace Pta\Backup\Providers;

use Illuminate\Support\ServiceProvider;
use Pta\Backup\Providers\PackageProvider;
use Pta\Backup\Providers\DataServiceProvider;
use Pta\Backup\Console\GenerateBackupCommand;

class BackupServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;
    
    /**
     * Providers to register
     *
     * @var array
     */
    protected $providers = [DataServiceProvider::class, PackageProvider::class];
    
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerBindings();
        
        $this->registerConfig();
        
        $this->registerViews();
        
        $this->registerTranslations();
        
        $this->registerMigration();
        
        $this->registerRoutes();

        $this->registerCommands();
    }
    
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        foreach ($this->providers as $provider) {
            $this->app->register($provider);
        }
        
        $this->registerConfig();
    }
    
    /**
     * Register Bindings in IoC.
     *
     * @return void
     */
    protected function registerBindings()
    {
    }
    
    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $configPath = realpath(__DIR__ . '/../../config/config.php');
        
        $this->publishes([ $configPath => config_path('backup.php'), 'config']);
        
        $this->mergeConfigFrom($configPath, 'backup');
    }
    
    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $this->loadViewsFrom(realpath(__DIR__ . '/../../resources/views'), 'pta/backup');
        
        $this->publishes([realpath(__DIR__ . '/../../resources/views') => base_path('resources/views/vendor/pta/backup'), ]);
    }
    
    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $this->loadTranslationsFrom(realpath(__DIR__ . '/../../resources/lang'), 'pta/backup');
    }
    
    public function registerMigration()
    {
        $this->publishes([realpath(__DIR__ . '/../../database/migrations') => database_path('/migrations') ], 'migrations');
    }
    
    public function populateMenus()
    {
    }
    
    public function registerRoutes()
    {
        $router = $this->app['router'];
        
        $prefix = $this->app['config']->get('backup.route_prefix', 'dashboard');
        
        $security = $this->app['config']->get('backup.security.protected', true);
        
        if (!$this->app->routesAreCached()) {
            $group = [];
            
            $group['prefix'] = $prefix;
            
            if ($security) {
                $middleware = $this->app['config']->get('backup.security.middleware');
                $permissions = $this->app['config']->get('backup.security.permission_name');
                
                $group['middleware'] = $this->app['config']->get('backup.security.middleware', $middleware);
                $group['can'] = $this->app['config']->get('backup.security.permission_name', $permissions);
            }
            
            $router->group($group, function () use ($router) {

                require realpath(__DIR__. '/../Http/routes.php');
            });
        }
    }

    protected function registerCommands()
    {
        $this->commands([
            GenerateBackupCommand::class
        ]);
    }
    
    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }
}
