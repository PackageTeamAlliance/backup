<?php

namespace Pta\Backup\Providers;

use Illuminate\Support\ServiceProvider;
use BackupManager\Laravel\Laravel5ServiceProvider;

class PackageProvider extends ServiceProvider
{
    /**
     * {@inheritDoc}
     */
    public function boot()
    {
        //
    }
    
    /**
     * {@inheritDoc}
     */
    public function register()
    {
        //register backup repository
        $this->app->register(Laravel5ServiceProvider::class);
    }
}
