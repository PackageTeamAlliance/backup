<?php

$router->group(['namespace' => 'Pta\Backup\Http\Controllers\Admin'], function () use ($router) {
  $router->get('/',  ['as' => 'backend.backup.index', 'uses' => 'BackupController@index']);
  $router->post('create',  ['as' => 'backend.backup.create', 'uses' => 'BackupController@backup']);
  $router->post('{id}/download',  ['as' => 'backend.backup.download', 'uses' => 'BackupController@download']);
});
