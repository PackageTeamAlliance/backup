<?php

namespace Pta\Backup\Http\Controllers\Admin;

use Illuminate\Container\Container;
use Illuminate\Support\Facades\Storage;
use Pta\Backup\Http\Controllers\Controller;
use Pta\Backup\Http\Requests\BackupCreateRequest;
use Pta\Backup\Http\Requests\BackupDownloadRequest;
use Pta\Backup\Repositories\BackupRepositoryInterface;

class BackupController extends Controller
{
    private $backups;
    private $app;
    private $manager;

    public function __construct(Container $app, BackupRepositoryInterface $backups)
    {
        $this->backups = $backups;
        
        $this->app = $app;

        $this->manager = $app['filesystem'];
    }

    public function index()
    {
        $backups = $this->backups->getBackups();

        return $this->getView('admin.index', compact('backups'));
    }

    public function backup(BackupCreateRequest $request)
    {
        $driver = $request->get('driver');
        
        $database = $request->get('database', 'mysql');

        $this->backups->makeBackup($driver, $database);

        return redirect(route('backend.backup.index'))->withFlashSuccess("Successfully created");
    }

    public function download(BackupDownloadRequest $request, $id)
    {
        $backup = $this->backups->getBackup($id);

        if ($this->manager->disk($backup->driver)->has($backup->name)) {
            
            $file = $this->manager->disk($backup->driver)->get($backup->name);

            $response = response($file, 200, [
                'Content-Type' => $this->manager->disk($backup->driver)->mimeType($backup->name),
                'Content-Length' => $this->manager->disk($backup->driver)->size($backup->name),
                'Content-Description' => 'File Transfer',
                'Content-Disposition' => "attachment; filename={$backup->name}",
                'Content-Transfer-Encoding' => 'binary',
            ]);

            ob_end_clean(); // <- this is important, i have forgotten why.

            return $response;
        }
    }
}
