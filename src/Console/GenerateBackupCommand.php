<?php

namespace Pta\Backup\Console;

use Illuminate\Console\Command;
use Pta\Backup\Repositories\BackupRepositoryInterface;

class GenerateBackupCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'core:backup {driver} {database}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Core Backup DB command';
    

    /**
     * The back up repository
     *
     * @var string
     */
    protected $backup;
    

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(BackupRepositoryInterface $backup)
    {
        parent::__construct();

        $this->backup = $backup;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $driver = $this->argument('driver');
        $database = $this->argument('database');

        $this->info("starting back up using the {$driver} to back up {$database} database");
        $this->backup->makeBackup($driver, $database);
        $this->info("completed back up using the {$driver} to back up {$database} database");
    }
}
