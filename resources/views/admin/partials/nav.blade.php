@permission(config('backup.permissions.view.name'))
<li class="{{ Active::pattern('admin/backup*') }} treeview">

	<a href="#">
		<span>Backups</span>
		<i class="fa fa-angle-left pull-right"></i>
	</a>
	<ul class="treeview-menu {{ Active::pattern('admin/backup*', 'menu-open') }}" style="display: none; {{ Active::pattern('admin/backup*', 'display: block;') }}">
		<li class="{{ Active::pattern('admin/backup') }}">
			<a href="{!! route('backend.backup.index') !!}"><i class="fa fa-database"></i> All Backups</a>
		</li>
	</ul>
</li>
@endauth