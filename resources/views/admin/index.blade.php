@extends ('backend.layouts.master')

@section('page-title')
{{ trans('pta/backup::general.title.index')}}
@stop

@section('page-header')
<h1>
	Backup Manager
	<small>For Amazon S3 and Local file drivers</small>
</h1>
@endsection

@section ('breadcrumbs')
<li><a href="{!!route('admin.dashboard')!!}"><i class="fa fa-dashboard"></i> {{ trans('menus.dashboard') }}</a></li>
<li class="active">Backups</li>
@stop

@section('main-panel-title')
{{ trans('pta/backup::general.header.index')}}
@stop


@section('after-styles-end')
@stop


@section('content')
<div class="box box-success">
	<div class="box-body">
		<div class="row">
			{!! BootForm::open()->post()->action(route('backend.backup.create')) !!}
			<div class="col-md-3">
				{!! BootForm::select('Driver', 'driver')->options(config('backup.disks')) !!}

				{{-- {!! BootForm::select('Database', 'database')->options(['mysql' => 'MySql']) !!} --}}

				<div class="form-group">
					<label class="control-label" for="driver">Create</label><br>
					{!! BootForm::submit('Create Back up')->class('btn btn-success')->help('Create') !!}
				</div>
			</div>

			<div class="col-md-3">
				
			</div>
			{!! BootForm::close() !!}

			
			<div class="col-md-12">
				<hr>
			</div>

			<div class="col-md-12">
				<table class="table table-stripped table-bordered">
					<thead>
						<tr>
							<td>ID</td>
							<td>Driver</td>
							<td>FileName</td>
							<td>Created At</td>
							<td>Download</td>
						</tr>
					</thead>
					<tbody>
						@foreach($backups as $backup)
							<tr>
								<td>{{$backup->id}}</td>
								<td> <span class="label label-success">{{$backup->driver}}</span> </td>
								<td> <span class="label label-primary">{{$backup->name}}</span> </td>
								<td>{{$backup->created_at->format('l F jS Y @ h:i:s A')}}</td>
								<td>
									{!! BootForm::open()->post()->action(route('backend.backup.download', [$backup->id]) ) !!}
									<button class="btn-primary btn">Download <i class="fa fa-download"></i></button>
									{!! BootForm::close() !!}
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>	
		</div>
	</div>
</div>
</div>
@stop

@section('scripts')
@stop