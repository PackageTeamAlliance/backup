<?php 

return [
    
    'name' => 'Backup',

   /*
     * Default prefix to the dashboard.
     */
    'route_prefix' => config('core.admin_uri') . 'backup',
    
    /*
     * Default permission user should have to access the dashboard.
     */
    'security' => [
        'protected'          => true,
        'middleware'         => ['web', 'access.routeNeedsPermission:manage-backups'],
        'permission_name'    => 'manage-backups',
    ],

    'permissions' => [
        'view' => [
            'name' => 'manage-backups',
            'display_name' => 'Manage Backups'
        ],
        'create' => [
            'name' => 'create-backups',
            'display_name' => 'Create Backups'
        ],
        'download' => [
            'name' => 'download-backups',
            'display_name' => 'Download Backups'
        ],
        'delete' => [
            'name' => 'delete-backups',
            'display_name' => 'Delete Backups'
        ],
    ],

    'disks' => [
       'local' => 'Local Filesystem',
       's3' => 'Amazon S3', 
    ],
    /*
     * Default url used to redirect user to front/admin of your the system.
     */
   'system_url' => config('core.redirect_url'),
];